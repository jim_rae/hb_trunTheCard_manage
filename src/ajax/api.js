import http from './httpSettings'

const api = {
  //获取配置信息
  getCommonConfig () {
    return http.get('https://huabao.cdollar.cn/hbfund-alipay/game/v1/commonConfig')
      .then(data => {
          return Promise.resolve(data)
      })
      .catch(err => {
          return Promise.reject(err)
      })
  },

  //更新图片
  updateCommonImg (key, url) {
    return http.post('updateCommonImg', {
        elementName: key,
        elementImgUrl: url
      })
      .then(data => {
        return Promise.resolve(data)
      })
      .catch(err => {
        return Promise.reject(err)
      })
  },

  //获取奖品列表
  getPrizeList () {
    return http.get('viewAllPrizeInfo')
      .then(data => {
          return Promise.resolve(data.prizeInfo)
      })
      .catch(err => {
          return Promise.reject(err)
      })
  },

  //添加奖品
  addPrize (prizeName, prizeImgUrl) {
    return http.post('addPrizeInfo', {
      prizeName, 
      prizeImgUrl
    })
    .then(data => {
      return Promise.resolve(data.data)
    })
    .catch(err => {
        return Promise.reject(err)
    })
  },

  //删除奖品
  deletePrize (id) {
    return http.post('deletePrizeInfo', {
      id
    })
    .then(data => {
      return Promise.resolve(data.data)
    })
    .catch(err => {
        return Promise.reject(err)
    })
  },

  //更新奖品
  updatePrize (id, prizeName, prizeImgUrl) {
    return http.post('updatePrizeInfo', {
      id,
      prizeName, 
      prizeImgUrl
    })
    .then(data => {
      return Promise.resolve(data.data)
    })
    .catch(err => {
        return Promise.reject(err)
    })
  },

  //获取成分股列表
  getFundList () {
    return http.get('viewAllFundInfo')
      .then(data => {
          return Promise.resolve(data.fundInfo)
      })
      .catch(err => {
          return Promise.reject(err)
      })
  },

  //添加成分股
  addFund (fundName, fundImgUrl, fundIntroduce) {
    return http.post('addFundInfo', {
      fundName, 
      fundImgUrl,
      fundIntroduce
    })
    .then(data => {
      return Promise.resolve(data.data)
    })
    .catch(err => {
        return Promise.reject(err)
    })
  },

  //删除成分股
  deleteFund (id) {
    return http.post('deleteFundInfo', {
      id
    })
    .then(data => {
      return Promise.resolve(data.data)
    })
    .catch(err => {
        return Promise.reject(err)
    })
  },

  //更新成分股
  updateFund (id, fundName, fundImgUrl, fundIntroduce) {
    return http.post('updateFundInfo', {
      id,
      fundName, 
      fundImgUrl,
      fundIntroduce
    })
    .then(data => {
      return Promise.resolve(data.data)
    })
    .catch(err => {
        return Promise.reject(err)
    })
  },

  //配置游戏信息
  confGameInfo (data) {
    return http.post('confGameInfo', data)
    .then(data => {
      return Promise.resolve(data.data)
    })
    .catch(err => {
        return Promise.reject(err)
    }) 
  },

  //获取当前抽奖奖品配置
  getDrawPrizeConfig () {
    return http.get('viewLuckyDrawPrize')
      .then(data => {
          return Promise.resolve(data)
      })
      .catch(err => {
          return Promise.reject(err)
      })
  },

  //获取当前游戏奖品配置
  getGamePrizeConfig () {
    return http.get('viewDailyPrize')
      .then(data => {
          return Promise.resolve(data)
      })
      .catch(err => {
          return Promise.reject(err)
      })
  },

  //配置抽奖奖品
  confLuckyDraw (data) {
    return http.post('confLuckyDraw', data)
    .then(data => {
      return Promise.resolve(data.code)
    })
    .catch(err => {
        return Promise.reject(err)
    }) 
  },

  //配置游戏奖品
  confGamePrize (data) {
    return http.post('confGamePrize', data)
    .then(data => {
      return Promise.resolve(data.code)
    })
    .catch(err => {
        return Promise.reject(err)
    }) 
  }
}

export default {
  install (Vue) {
    Vue.mixin({
      created () {
        this.$_api = api
      }
    })
  }
}
