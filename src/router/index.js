import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const Login = () => import('@/page/login')
const Manage = () => import('@/page/manage')
const ImgSetHome = () => import('@/page/img-set-home')
const ImgSetGame = () => import('@/page/img-set-game')
const ImgSetAlert = () => import('@/page/img-set-alert')
const ImgSetAward = () => import('@/page/img-set-award')
const ImgSetDraw = () => import('@/page/img-set-draw')
const ImgSetRank = () => import('@/page/img-set-rank')
const AwardSetList = () => import('@/page/award-set-list')
const AwardSetRank= () => import('@/page/award-set-rank')
const AwardSetDraw = () => import('@/page/award-set-draw')
const GameSetFundList = () => import('@/page/game-set-fund-list')
const GameSetOne = () => import('@/page/game-set-one')
const GameSetTwo = () => import('@/page/game-set-two')
const GameSetThree = () => import('@/page/game-set-three')
const CheckMore = () => import('@/page/check-more')
const BannerConfig = () => import('@/page/banner-config')

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/manage',
      redirect: '/imgSetHome'
    },
    {
      path: '/imgSetHome',
      name: 'Manage',
      component: Manage,
      children: [
        {
          path: '',
          name: 'ImgSetHome',
          component: ImgSetHome,
          meta: ['页面图片配置', '首页']
        },
        {
          path: '/imgSetGame',
          name: 'ImgSetGame',
          component: ImgSetGame,
          meta: ['页面图片配置', '游戏页']
        },
        {
          path: '/imgSetAlert',
          name: 'ImgSetAlert',
          component: ImgSetAlert,
          meta: ['页面图片配置', '弹窗']
        },
        {
          path: '/imgSetAward',
          name: 'ImgSetAward',
          component: ImgSetAward,
          meta: ['页面图片配置', '我的奖品页']
        },
        {
          path: '/imgSetDraw',
          name: 'ImgSetDraw',
          component: ImgSetDraw,
          meta: ['页面图片配置', '抽奖页']
        },
        {
          path: '/imgSetRank',
          name: 'ImgSetRank',
          component: ImgSetRank,
          meta: ['页面图片配置', '排行榜页']
        },
        {
          path: '/awardSetList',
          name: 'AwardSetList',
          component: AwardSetList,
          meta: ['奖品配置', '奖品列表']
        },
        {
          path: '/awardSetRank',
          name: 'AwardSetRank',
          component: AwardSetRank,
          meta: ['奖品配置', '排行榜']
        },
        {
          path: '/awardSetDraw',
          name: 'AwardSetDraw',
          component: AwardSetDraw,
          meta: ['奖品配置', '周二抽奖']
        },
        {
          path: '/gameSetFundList',
          name: 'GameSetFundList',
          component: GameSetFundList,
          meta: ['游戏配置', '成分股列表']
        },
        {
          path: '/gameSetOne',
          name: 'GameSetOne',
          component: GameSetOne,
          meta: ['游戏配置', '第一关']
        },
        {
          path: '/gameSetTwo',
          name: 'GameSetTwo',
          component: GameSetTwo,
          meta: ['游戏配置', '第二关']
        },
        {
          path: '/gameSetThree',
          name: 'GameSetThree',
          component: GameSetThree,
          meta: ['游戏配置', '第三关']
        },
        {
          path: '/checkMore',
          name: 'CheckMore',
          component: CheckMore,
          meta: ['链接及banner配置', '首页查看更多链接']
        },
        {
          path: '/bannerConfig',
          name: 'BannerConfig',
          component: BannerConfig,
          meta: ['链接及banner配置', 'banner']
        }
      ]
    }
  ]
})
