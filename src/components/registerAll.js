import ImgSetItem from './img-set-item'
import HeadTop from './head-top'

const components = {
  ImgSetItem,
  HeadTop
}

export default function registerAllComponents (instance) {
  Object.keys(components).forEach(key => {
    instance.component(key, components[key])
  })
}